const config = require('../config');

const Sequelize = require('sequelize');
const sequelize = new Sequelize(config.dataBase.url);

const wavesNode = config.wavesNode;

const axios = require('axios');

const Transaction = sequelize.import("../Models/transaction.js");
Transaction.sync();

const headers = {
    'Content-Type': 'application/json',
    'Accept': 'application/json',
    'api_key': wavesNode.api_key
};

class TransactionManager {
    constructor() {
    }

    static sendWaves(user, recipientAddress, amount) {
        const url = wavesNode.url + wavesNode.transferPath;
        let post_data = JSON.stringify({
            'assetId': null,
            'amount': amount*100000000,
            'fee': 100000,
            'sender': user.address,
            'attachment': '',
            'recipient': recipientAddress
        });
        axios.post(url, post_data, headers)
            .then(
                // Transaction.create({})
                console.log("Then w sendWaves")
            )
            .catch(err => {
                console.log("caught: ", err);
            });
    }
}
module.exports = TransactionManager;
