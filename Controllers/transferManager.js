const config = require('../config');

const wavesNode = config.wavesNode;
const wavesNodePost = config.wavesNodePost;

const axios = require('axios');

const Token = require('./tokenManager.js');
function makeConfig() {
    return {
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'api_key': wavesNode.api_key
        }
    }
}

class TransferManager {
    constructor() {
        this.config = this.makeConfig();
        this.url = wavesNode.url;
    }

     async getAssetId(assetName) {
        return await Token.getAssetId(assetName);
    }



    static makeTransactionsFromFile(path){
        let that = this;
        let lineReader = require('readline').createInterface({
            input: require('fs').createReadStream(path)
        });
        lineReader.on('line', function (line) {
            let tab = line.split(",");
            tab[2] = parseInt(tab[2]);
            that.sendTransaction(tab[0], tab[1], tab[2]);
        });
    }

    static sendTransaction(addressId, assetId, amount) {
        const url = wavesNode.url + wavesNodePost.transferPath;
        let post_data = JSON.stringify({
            'assetId': assetId,
            'amount': amount,
            'fee': 100000,
            'sender': wavesNode.mainAddress,
            'attachment': '',
            'recipient': addressId
        });
        axios.post(url, post_data, makeConfig())
            .catch(err => {
                console.log("caught: ", err);
        });
    }

}
module.exports = TransferManager;