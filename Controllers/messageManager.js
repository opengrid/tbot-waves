const config = require('../config');

const Sequelize = require('sequelize');
const sequelize = new Sequelize(config.dataBase.url);

const User = require('./userManager.js');
const MessageLog = require('./messageLogManager.js');
const Waves = require('./wavesManager.js');
const Transaction = require('./transactionManager');


/*callback: (callbackQuery, message) => {
    $.api.editMessageText('new text', { chat_id: $.chatId, message_id: message.messageId })*/

class MessageManager
{

    static async startMenu($, messageId=null){
        let options = [];
        let user = await User.getUser($);
        if(user){
            options.push({text: 'User name', callback: (callbackQuery, message) => {this.userMenu($, message.messageId);}});
        } else {
            options.push({text: 'I want to create an account', callback: (callbackQuery, message) => { this.createAccount($, message.messageId);}});
        }
        options.push({text: 'FAQ', callback: (callbackQuery, message) => { this.getFaq($, message.messageId);}});
        if(messageId){
            const msg = '[Start menu]\nSelect one of these options:';
            $.api.deleteMessage($.chatId, messageId);
            $.runInlineMenu({
                layout: 1,
                method: 'sendMessage',
                params: [msg],
                menu: options
            });
        } else {
            const msg = '[Start menu]\nWelcome, this telegram bot will help you manage your Wavesplatform account.\nSelect one of these options:';
            $.runInlineMenu({
                layout: 1,
                method: 'sendMessage',
                params: [msg],
                menu: options
            });
        }
    }

    static async createAccount($, messageId){
        let options = [{
            text: 'Yes', callback: async (callbackQuery, message)=>{
                await User.createAccount($, message.messageId); this.userMenu($, message.messageId) }
        },{
            text: 'No', callback: (callbackQuery, message)=>{this.startMenu($, message.messageId);}
        }];
        const msg = 'Are you sure you want to create an account?';
        $.api.deleteMessage($.chatId, messageId);
        $.runInlineMenu({
            layout: 1,
            method: 'sendMessage',
            params: [msg],
            menu: options
        });
    }

    static async getFaq($, messageId){
        let options = [{
            text: 'Back', callback: (callbackQuery, message)=>{this.startMenu($, message.messageId); }
        }];
        const msg = '[FAQ]\nFrequently asked questions';
        $.api.deleteMessage($.chatId, messageId);
        $.runInlineMenu({
            layout: 1,
            method: 'sendMessage',
            params: [msg],
            menu: options
        });
    }

    static async userMenu($, messageId){
        let user = await User.getUser($);
        let msg = '';
        let options = [];
        if(user){
            msg = "[User menu]\nImię: "+user.first_name+", Last Name: "+user.last_name+", Firstname: "+user.username+
                "\nWaves address: "+user.address;
            options = [{
                text: 'Balance', callback: (callbackQuery, message)=>{ this.getAccountBalance($, message.messageId, user.address);}
            },{
                text: 'Make transfer', callback: (callbackQuery, message)=>{ this.makeTransfer($, message.messageId, user);}
            },{
                text: 'Your tokens', callback: (callbackQuery, message)=>{ this.yourTokens($, message.messageId);}
            },{
                text: 'Address Book', callback: (callbackQuery, message)=>{ this.addressBook($, message.messageId, user);}
            },{
                text: 'Account options', callback: (callbackQuery, message)=>{ this.editAccountData($, message.messageId, user);}
            }];
        } else {
            msg = "[User menu]\nYou don't have an account";
            options = [{
                text: 'Back to main menu', callback: (callbackQuery, message)=>{ this.startMenu($, message.messageId);}
            }];
        }
        $.api.deleteMessage($.chatId, messageId);
        $.runInlineMenu({
            layout: 2,
            method: 'sendMessage',
            params: [msg],
            menu: options
        });
    }

    //tokeny użytkownika
    static async yourTokens($, messageId){
        const msg = "[Your tokens]\nNot available at the moment";
        let options = [{
            text: 'Back', callback: (callbackQuery, message)=>{this.userMenu($, message.messageId);}
        }];
        $.api.deleteMessage($.chatId, messageId);
        $.runInlineMenu({
            layout: 1,
            method: 'sendMessage',
            params: [msg],
            menu: options
        });
    }

    //edycja konta
    static async editAccountData($, messageId, user){
        let msg = "[Account options]\nFirstname: "+user.first_name+", Lastname: "+user.last_name+", Username: "+user.username+
            "\nWaves address: "+user.address;
        let options = [{
            text: 'Change firstname', callback: (callbackQuery, message)=>{ this.accountEdit($, message.messageId, user,1);}
        },{
            text: 'Change lastname', callback: (callbackQuery, message)=>{ this.accountEdit($, message.messageId, user,2);}
        },{
            text: 'Change username', callback: (callbackQuery, message)=>{ this.accountEdit($, message.messageId, user,3);}
        },{
            text: 'Delete account', callback: (callbackQuery, message)=>{ this.accountDelete($, message.messageId, user);}
        },{
            text: 'Back', callback: (callbackQuery, message)=>{ this.userMenu($, message.messageId);}
        }];
        $.api.deleteMessage($.chatId, messageId);
        $.runInlineMenu({
            layout: 2,
            method: 'sendMessage',
            params: [msg],
            menu: options
        });
    }

    static async accountEdit($, messageId, user, option){
        let msg = "[Account modification]\nGive a new ";
        if(option === 1)
            msg += "firstname";
        else if(option === 2)
            msg += "lastname";
        else
            msg += "username";
        msg += "(max. 20 characters):";
        $.api.editMessageText(msg,{ chat_id: $.chatId, message_id: messageId });
        $.waitForRequest
            .then(async $ => {
                msg = '';
                let options = [];
                $.api.editMessageText("[Account modification]\nNew name given" ,{ chat_id: $.chatId, message_id: messageId });
                let name = $.message.text;
                if(name.length <= 20){
                    msg = "[Account - edit]\nSure to make a change?\n";
                    options.push({text: 'Yes', callback: async (callbackQuery, message)=>{
                        let data;
                        if(option === 1)
                            data = await User.updateFirstName(user, name);
                        else if(option === 2)
                            data = await User.updateLastName(user, name);
                        else
                            data = await User.updateUsername(user, name);
                        this.editAccountData($, message.messageId, data)}},
                        {text: 'No', callback: (callbackQuery, message)=>{this.editAccountData($, message.messageId, user)}});
                } else {
                    msg = "[Account - edit]\nName is invalid";
                    options.push({text: 'Back', callback: (callbackQuery, message)=>{this.editAccountData($, message.messageId, user)}});
                }
                $.runInlineMenu({
                    layout: 1,
                    method: 'sendMessage',
                    params: [msg],
                    menu: options
                });
            })
    }

    static async accountDelete($, messageId, user){
        const msg = "[Account - usuwanie]\nAre you sure?!";
        let options = [{
            text: 'Yes', callback: async (callbackQuery, message)=>{await User.deleteUser(user); this.startMenu($, message.messageId) }
        },{
            text: 'No', callback: (callbackQuery, message)=>{this.editAccountData($, message.messageId, user);}
        }];
        $.api.deleteMessage($.chatId, messageId);
        $.runInlineMenu({
            layout: 1,
            method: 'sendMessage',
            params: [msg],
            menu: options
        });
    }

    //saldo
    static async getAccountBalance($, messageId, address){
        const msg = "[Balance]\nWaves: "+ await Waves.getAddressWavesBalance(address);
        let options = [{
            text: 'Token Balance', callback: async (callbackQuery, message)=>{await this.getAccountAssetsBalance($, message.messageId, address) }
        },{
            text: 'Back', callback: (callbackQuery, message)=>{this.userMenu($, message.messageId);}
        }];
        $.api.deleteMessage($.chatId, messageId);
        $.runInlineMenu({
            layout: 1,
            method: 'sendMessage',
            params: [msg],
            menu: options
        });
    }

    static async getAccountAssetsBalance($, messageId, address){
        const msg = "[Token Balance]\n"+ await Waves.getAddressAssetsBalance(address);
        let options = [{
            text: 'Back', callback: (callbackQuery, message)=>{this.getAccountBalance($, message.messageId, address);}
        }];
        $.api.deleteMessage($.chatId, messageId);
        $.runInlineMenu({
            layout: 1,
            method: 'sendMessage',
            params: [msg],
            menu: options
        });
    }

    //address book
    static async addressBook($, messageId, user){
        const msg = "[Address Book]\nChoose one of these options";
        let options = [{
            text: 'Add address', callback: (callbackQuery, message)=>{this.addressBookAddAddress($, message.messageId, user);}
        },{
            text: 'List of addresses', callback: (callbackQuery, message)=>{this.addressBookList($, message.messageId, user);}
        },{
            text: 'Back', callback: (callbackQuery, message)=>{this.userMenu($, message.messageId);}
        }];
        $.api.deleteMessage($.chatId, messageId);
        $.runInlineMenu({
            layout: 2,
            method: 'sendMessage',
            params: [msg],
            menu: options
        });
    }

    //ksiązka adresowa - dodawanie
    static async addressBookAddAddress($, messageId, user){
        $.api.editMessageText("[Address Book - add address]\nGive an address",{ chat_id: $.chatId, message_id: messageId });
        $.waitForRequest
            .then(async $ => {
                let msg = '';
                let options = [];
                $.api.editMessageText("[Address Book - add address]\n Chosen option: Add address" ,{ chat_id: $.chatId, message_id: messageId });
                let address = $.message.text;
                console.log(address);
                if(await Waves.validateAddress(address)){
                    msg = "[Address Book - add address]\nAre you sure to add this address:\n"+address;
                    options.push({text: 'Yes', callback: (callbackQuery, message)=>{this.addressBookAddName($, message.messageId, user, address)}},
                        {text: 'No', callback: (callbackQuery, message)=>{this.addressBook($, message.messageId, user)}});
                } else {
                    msg = "[Address Book - add address]\nAddress invalid";
                    options.push({text: 'Back', callback: (callbackQuery, message)=>{this.addressBook($, message.messageId, user)}});
                }
                $.runInlineMenu({
                    layout: 1,
                    method: 'sendMessage',
                    params: [msg],
                    menu: options
                });
            })
    }

    static async addressBookAddName($, messageId, user, address, amount = 0){
        $.api.editMessageText("[Address Book - add address]\nGiva a name max 20 chars:",{ chat_id: $.chatId, message_id: messageId });
        $.waitForRequest
            .then(async $ => {
                let msg = '';
                let options = [];
                $.api.editMessageText("[Address Book - add address]\nName given" ,{ chat_id: $.chatId, message_id: messageId });
                let name = $.message.text;
                console.log(name);
                if(name.length <= 20){
                    msg = "[Address Book - give a name]\nAre you sure to add this address?\nAddress: "+address+"\nName: "+name;
                    options.push({text: 'Yes', callback: async (callbackQuery, message)=>{let data = await User.addAddress(user, address, name); amount === 0 ? this.addressBookAddFinalise($, message.messageId, user, data) : this.transferWavesFinalize($, message.messageId, user, address, amount)}},
                        {text: 'No', callback: (callbackQuery, message)=>{amount === 0 ? this.addressBook($, message.messageId, user) : this.transferWavesFinalize($, message.messageId, user, address, amount)}});
                    if(amount !== 0)
                        options.push({text: 'Give a name one again', callback: async (callbackQuery, message)=>{this.addressBookAddName($, message.messageId, user, address, amount);}})
                } else {
                    msg = "[Address Book - give a name]\nInvalid name given";
                    options.push({text: 'Back', callback: (callbackQuery, message)=>{amount === 0 ? this.addressBook($, message.messageId, user) : this.addressBookAddName($, message.messageId, user, address, amount)}});
                }
                $.runInlineMenu({
                    layout: 1,
                    method: 'sendMessage',
                    params: [msg],
                    menu: options
                });
            })
    }

    static async addressBookAddFinalise($, messageId, user, address){
        const msg = "[Address Book - address added]\nAddress: "+address.address+"\nName: "+address.name;
        let options = [{
            text: 'Go to address book', callback: (callbackQuery, message)=>{this.addressBook($, message.messageId, user);}
        },{
            text: 'Go to user menu', callback: (callbackQuery, message)=>{this.userMenu($, message.messageId);}
        }];
        $.api.deleteMessage($.chatId, messageId);
        $.runInlineMenu({
            layout: 1,
            method: 'sendMessage',
            params: [msg],
            menu: options
        });
    }

    //address book - lista
    static async addressBookList($, messageId, user){
        const msg = "[Address Book - lista]\nChoose option:";
        let options = [];
        const list = await User.getUserAddressBook(user);
        console.log(list);
        for(let i=0; i<list.length; i++){
            options.push({text: list[i].name, callback: (callbackQuery, message)=>{this.addressBookAddress($, message.messageId, user, list[i])}});
        }
        options.push({text: 'Add new address', callback: (callbackQuery, message)=>{this.addressBookAddAddress($, message.messageId, user);}},
            {text: 'Back', callback: (callbackQuery, message)=>{this.addressBook($, message.messageId, user)}});
        $.api.deleteMessage($.chatId, messageId);
        $.runInlineMenu({
            layout: 3,
            method: 'sendMessage',
            params: [msg],
            menu: options
        });
    }

    //address book - adres
    static async addressBookAddress($, messageId, user, address){
        const msg = "[Adres]\nName: "+address.name+"\nAddress: "+address.address;
        let options = [{
            text: 'Edit name', callback: (callbackQuery, message)=>{this.addressBookEditAddress($, message.messageId, user, address);}
        },{
            text: 'Delete', callback: (callbackQuery, message)=>{this.addressBookDeleteAddress($, message.messageId, user, address);}
        },{
            text: 'Back', callback: (callbackQuery, message)=>{this.addressBookList($, message.messageId, user);}
        }];
        $.api.deleteMessage($.chatId, messageId);
        $.runInlineMenu({
            layout: 1,
            method: 'sendMessage',
            params: [msg],
            menu: options
        });
    }

    static async addressBookEditAddress($, messageId, user, address){
        $.api.editMessageText("[Adres - Edit]\nGive a new name max 20 chars",{ chat_id: $.chatId, message_id: messageId });
        $.waitForRequest
            .then(async $ => {
                let msg = '';
                let options = [];
                $.api.editMessageText("[Adres - Edit]\nName given" ,{ chat_id: $.chatId, message_id: messageId });
                let name = $.message.text;
                console.log(name);
                if(name.length <= 20){
                    msg = "[Adres - Edit]\nAre you sure to add address:\nAddress: "+address.address+"\nName: "+name;
                    options.push({text: 'Yes', callback: async (callbackQuery, message)=>{let data = await User.updateAddress(address, name); this.addressBookAddress($, message.messageId, user, data)}},
                        {text: 'No', callback: (callbackQuery, message)=>{this.addressBookAddress($, message.messageId, user, address)}});
                } else {
                    msg = "[Adres - Edit]\nInvalid name given";
                    options.push({text: 'Back', callback: (callbackQuery, message)=>{this.addressBookAddress($, message.messageId, user, address)}});
                }
                $.runInlineMenu({
                    layout: 1,
                    method: 'sendMessage',
                    params: [msg],
                    menu: options
                });
            })
    }

    static async addressBookDeleteAddress($, messageId, user, address){
        const msg = "[Adres - Removing]\nAre you sure to delete?\nName: "+address.name+"\nAddress: "+address.address;
        let options = [{
            text: 'Yes', callback: async (callbackQuery, message)=>{await User.deleteAddress(address); this.addressBookList($, message.messageId, user);}
        },{
            text: 'No', callback: (callbackQuery, message)=>{this.addressBookAddress($, message.messageId, user, address);}
        }];
        $.api.deleteMessage($.chatId, messageId);
        $.runInlineMenu({
            layout: 1,
            method: 'sendMessage',
            params: [msg],
            menu: options
        });
    }


    //transfer
    static async makeTransfer($, messageId, user){
        let balance = await Waves.getAddressWavesBalance(user.address);
        let msg = '';
        let options = [];
        if(balance < 0.001){
            msg = "[Transfer]\nNot enough funds\n" +
                "Minimum 0.001 waves\n" +
                "Available funds: "+balance;
        } else {
            msg = "[Transfer]\nWhat transfer you want to make?";
            options.push({text: 'Waves', callback: (callbackQuery, message)=>{this.transferWavesAmount($, message.messageId, user, balance)}},
                {text: 'tokens', callback: (callbackQuery, message)=>{this.transferAssets($, message.messageId, user)}});
        }
        options.push({text: 'Back', callback: (callbackQuery, message)=>{this.userMenu($, message.messageId);}});
        $.api.deleteMessage($.chatId, messageId);
        $.runInlineMenu({
            layout: 2,
            method: 'sendMessage',
            params: [msg],
            menu: options
        });
    }

    static async transferWavesAmount($, messageId, user, balance){
        let msg = "[Transfer Waves]\nHow much (costs 0.001 waves)\nAvailable funds: "+balance+" waves";
        let options = [];
        const availableBalance = balance - 0.001;
        if(availableBalance >= 0.01)
            options.push({text: '0.01', callback: (callbackQuery, message)=>{this.transferWavesAddress($, message.messageId, user, 0.01)}});
        if(availableBalance >= 0.05)
            options.push({text: '0.05', callback: (callbackQuery, message)=>{this.transferWavesAddress($, message.messageId, user, 0.05)}});
        if(availableBalance >= 0.1)
            options.push({text: '0.1', callback: (callbackQuery, message)=>{this.transferWavesAddress($, message.messageId, user, 0.1)}});
        if(availableBalance >= 0.5)
            options.push({text: '0.5', callback: (callbackQuery, message)=>{this.transferWavesAddress($, message.messageId, user, 0.5)}});
        if(availableBalance >= 1)
            options.push({text: '1', callback: (callbackQuery, message)=>{this.transferWavesAddress($, message.messageId, user, 1)}});
        options.push({text: 'Other amount', callback: (callbackQuery, message)=>{this.transferWavesCustomAmount($, message.messageId, user, balance)}},
            {text: 'Back', callback: (callbackQuery, message)=>{this.makeTransfer($, message.messageId, user)}});
        $.api.deleteMessage($.chatId, messageId);
        $.runInlineMenu({
            layout: 2,
            method: 'sendMessage',
            params: [msg],
            menu: options
        });

    }

    static async transferWavesCustomAmount($, messageId, user, balance){
        $.api.editMessageText("[Transfer Waves - other amount]\nAvailable funds: "+balance+" waves\nHow much:\ni.e. 0.15" ,{ chat_id: $.chatId, message_id: messageId });
        $.waitForRequest
            .then($ => {
                let msg = '';
                let options = [];
                $.api.editMessageText("[Transfer Waves - other amount]\n Chosen option: Other amount" ,{ chat_id: $.chatId, message_id: messageId });
                let amount = $.message.text;
                console.log(amount);
                amount = parseFloat(amount);
                console.log(amount);
                if(amount <= 0.001){
                    msg = "[Transfer Waves - other amount]\nToo low";
                    options.push({text: 'Back', callback: (callbackQuery, message)=>{this.transferWavesAmount($, message.messageId, user, balance)}});
                } else if(isNaN(amount)){
                    msg = "[Transfer Waves - other amount]\nInvalid";
                    options.push({text: 'Back', callback: (callbackQuery, message)=>{this.transferWavesAmount($, message.messageId, user, balance)}});
                } else if(amount >= balance - 0.001){
                    msg = "[Transfer Waves - other amount]\nToo much";
                    options.push({text: 'Back', callback: (callbackQuery, message)=>{this.transferWavesAmount($, message.messageId, user, balance)}});
                } else {
                    msg = "[Transfer Waves - other amount]\nAre you sure to transfer "+amount+" waves?";
                    options.push({text: 'Yes', callback: (callbackQuery, message)=>{this.transferWavesAddress($, message.messageId, user, amount)}});
                    options.push({text: 'No', callback: (callbackQuery, message)=>{this.transferWavesAmount($, message.messageId, user, balance)}});
                }
                $.runInlineMenu({
                    layout: 1,
                    method: 'sendMessage',
                    params: [msg],
                    menu: options
                });
            })
    }

    static async transferWavesAddress($, messageId, user, amount){
        let msg = "[Transfer Waves - address]\nChoose address:";
        let options = [{text: 'Choose from address book', callback: (callbackQuery, message)=>{this.transferWavesAddressBook($, message.messageId, user, amount);}},
            {text: 'Enter address by hand', callback: (callbackQuery, message)=>{this.transferWavesCustomAddress($, message.messageId, user, amount);}},
            {text: 'Back', callback: (callbackQuery, message)=>{this.makeTransfer($, message.messageId, user);}}];
        $.api.deleteMessage($.chatId, messageId);
        $.runInlineMenu({
            layout: 1,
            method: 'sendMessage',
            params: [msg],
            menu: options
        });
    }

    static async transferWavesAddressBook($, messageId, user, amount){
        let msg = "[Transfer Waves - address book]\nChoose address:";
        let options = [];
        const list = await User.getUserAddressBook(user);
        console.log(list);
        for(let i=0; i<list.length; i++){
            options.push({text: list[i].name, callback: (callbackQuery, message)=>{this.transferWavesFinalize($, message.messageId, user, list[i].address, amount)}});
        }
        options.push({text: 'Back', callback: (callbackQuery, message)=>{this.transferWavesAddress($, message.messageId, user, amount)}});
        $.api.deleteMessage($.chatId, messageId);
        $.runInlineMenu({
            layout: 3,
            method: 'sendMessage',
            params: [msg],
            menu: options
        });

    }

    static async transferWavesCustomAddress($, messageId, user, amount){
        $.api.editMessageText("[Transfer Waves - other address]\nChoose address:" ,{ chat_id: $.chatId, message_id: messageId });
        $.waitForRequest
            .then(async $ => {
                let msg = '';
                let options = [];
                $.api.editMessageText("[Transfer Waves]\n Chosen option: Other address" ,{ chat_id: $.chatId, message_id: messageId });
                let address = $.message.text;
                console.log(address);
                if(await Waves.validateAddress(address)){
                    msg = "[Transfer Waves - other address]\nAre you sure you select:\n"+address;
                    options.push({text: 'Yes', callback: (callbackQuery, message)=>{this.transferWavesAskAddressBook($, message.messageId, user, address, amount)}},
                        {text: 'No', callback: (callbackQuery, message)=>{this.transferWavesFinalize($, message.messageId, user, amount)}});
                } else {
                    msg = "[Transfer Waves - other address]\nAddress invalid";
                    options.push({text: 'Back', callback: (callbackQuery, message)=>{this.transferWavesAddress($, message.messageId, user, amount)}});
                }
                $.runInlineMenu({
                    layout: 1,
                    method: 'sendMessage',
                    params: [msg],
                    menu: options
                });
            })
    }

    static async transferWavesAskAddressBook($, messageId, user, address, amount){
        let msg = "[Transfer Waves - adres]\nAdd to address book?";
        let options = [{text: 'Yes', callback: (callbackQuery, message)=>{this.addressBookAddName($, message.messageId, user, address, amount);}},
            {text: 'No', callback: (callbackQuery, message)=>{this.transferWavesFinalize($, message.messageId, user, address, amount);}}];
        $.api.deleteMessage($.chatId, messageId);
        $.runInlineMenu({
            layout: 2,
            method: 'sendMessage',
            params: [msg],
            menu: options
        });
    }

    static async transferWavesFinalize($, messageId, user, address, amount){
        let msg = "[Transfer Waves - finalize]\nAmount: "+amount+"\nAddress: "+address+"\nAre you sure to make a transfer?";
        let options = [{text: 'Yes', callback: (callbackQuery, message)=>{Transaction.sendWaves(user, address, amount); this.makeTransfer($, message.messageId, user);}},
            {text: 'No', callback: (callbackQuery, message)=>{this.makeTransfer($, message.messageId, user);}}];
        $.api.deleteMessage($.chatId, messageId);
        $.runInlineMenu({
            layout: 2,
            method: 'sendMessage',
            params: [msg],
            menu: options
        });
    }


    static async transferAssets($, messageId, user){
        const msg = "[Token transfer]\nNot available at the moment";
        let options = [{
            text: 'Back', callback: (callbackQuery, message)=>{this.makeTransfer($, message.messageId, user);}
        }];
        $.api.deleteMessage($.chatId, messageId);
        $.runInlineMenu({
            layout: 1,
            method: 'sendMessage',
            params: [msg],
            menu: options
        });
    }

    static async testCommand($){
        // Waves.createNewAddress();
    }




}
module.exports = MessageManager;
