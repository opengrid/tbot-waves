const config = require('../config');

const Sequelize = require('sequelize');
const sequelize = new Sequelize(config.dataBase.url);

const Balance = require('./balanceManager.js');
const Group = require('./groupManager.js');

const Token = sequelize.import("../Models/token.js");
Token.sync();

class TokenManager {

    static help($){
        $.sendMessage('Command /token will show token name and ID\n' +
            'Available commands:\n' +
            '/token get - token details');
    }

    static async getInfo($){
        $.sendMessage('Input token name');
        $.waitForRequest.then(async $ =>{
            let token = await Token.findOne({where:{assetName: $.message.text}});
            if(token){
                $.sendMessage('Token:\nName: '+token.assetName+', id: '+token.assetId);
            } else
                $.sendMessage('No token found for a given name')
        })
    }

    static async getAssetId(assetName) {
        let token = await Token.findOne({where: {assetName: assetName}});
        if(token) {
            token = await token.get();
            return token.assetId;
        }
        return null;
    }

    static async getTokensFromBalance($){
        if(await Group.getUserPrivileges($)) {
            const balance = await Balance.getAssetsBalance();
            for (let i = 0; i < balance.length; i++) {
                let token = await Token.findOne({where: {assetId: balance[i].assetId}});
                if (token) {
                }
                else
                    Token.create({assetId: balance[i].assetId, assetName: balance[i].issueTransaction.name});
            }
        } else
            $.sendMessage('Permission denied')
    }

}

module.exports = TokenManager;
