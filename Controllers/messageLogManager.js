const config = require('../config');

const Sequelize = require('sequelize');
const sequelize = new Sequelize(config.dataBase.url);

const MessageLog = sequelize.import("../Models/messageLog.js");
MessageLog.sync();

class MessageLogManager
{
    static async getIds(message){
        return await MessageLog.findAll({where:{message: message}})
    }

    static async logData($) {
        // await User.addUser($);
        await MessageLog.create({userId: $.message.from.id, message: $.message.text});
    }
}
module.exports = MessageLogManager;
