const config = require('../config');
const wavesNode = config.wavesNode;

const axios = require('axios');

const headers = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'api_key': wavesNode.api_key
};

class WavesManager {
    constructor(){
    }

    static async createNewAddress(){
        const url = wavesNode.url + wavesNode.createAddressPath;
        let data = await axios.post(url, {}, {headers: headers})
            .catch(err => {
            console.log("caught: ", err)});
        return await data.data.address;
    }

    static async getAddressWavesBalance(address){
        let url = wavesNode.url + wavesNode.wavesBalancePath + address;
        let data = await axios.get(url);
        data = data.data.balance/100000000;
        return data;
    }

    static async getAddressAssetsBalance(address){
        let url = wavesNode.url + wavesNode.assetsBalancePath + address;
        let data = await axios.get(url);
        data = data.data.balances;
        return data;
    }

    static async validateAddress(address){
        let url = wavesNode.url + wavesNode.validateAddressPath + address;
        console.log(url);
        let data = await axios.get(url);
        data = data.data.valid;
        console.log(data);
        return data;
    }
}
module.exports = WavesManager;
