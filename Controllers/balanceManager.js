const config = require('../config');
const wavesNode = config.wavesNode;
const wavesNodeGet = config.wavesNodeGet;

const axios = require('axios');

class BalanceManager {
    constructor(){
    }

    static async getWavesBalanceInfo(){
        const url = wavesNode.url + wavesNodeGet.wavesBalancePath + wavesNode.mainAddress;
        let data = await axios.get(url);
        data = data.data;
        return 'Address: '+data.address+'\nWaves Balance: '+data.balance;
    }

    static async getAssetsBalanceInfo(){
        const url = wavesNode.url + wavesNodeGet.assetsBalancePath + wavesNode.mainAddress;
        let data = await axios.get(url);
        data = data.data;
        const assets = data.balances;
        let info = 'Address: '+data.address+'\n';
        for(let i = 0; i<assets.length; i++){
            let asset = assets[i];
            info += 'AssetId: '+asset.assetId+';AssetName: '+asset.issueTransaction.name+';Balance: '+asset.balance+'; \n';
        }
        return info;
    }

    static async getAssetBalance(assetId){
        const url = wavesNode.url + wavesNodeGet.assetsBalancePath + wavesNode.mainAddress;
        let data = await axios.get(url);
        data = data.data;
        const assets = data.balances;
        for(let i = 0; i<assets.length; i++){
            if(assets[i].assetId === assetId){
                return assets[i].balance;
            }
        }
    }

    static async getAssetsBalance(){
        const url = wavesNode.url + wavesNodeGet.assetsBalancePath + wavesNode.mainAddress;
        let data = await axios.get(url);
        data = data.data;
        return data.balances;
    }

}
module.exports = BalanceManager;
