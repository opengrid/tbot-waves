const config = require('../config');

const Sequelize = require('sequelize');
const sequelize = new Sequelize(config.dataBase.url);
const Op = Sequelize.Op;

const Waves = require("./wavesManager");
const User = sequelize.import("../Models/user.js");
const AddressBook = sequelize.import("../Models/addressBook.js");
User.sync();
AddressBook.sync();


class UserManager {
    constructor() {
    }

    static async createAccount($){
        let user = await User.findOne({where: {id: $.message.from.id}});
        if(user){
            user = await user.get();
            if(user.username){
                if(user.username === $.message.from.username){}
                else{
                    await User.update({username: $.message.from.username},{where:{id: $.message.from.id}});
                }}
            else
                await User.update({username: $.message.from.username},{where: {id: $.message.from.id}});
            return false;
        } else {
            await User.create({id: $.message.from.id, first_name: $.message.from.firstName,
                last_name: $.message.from.lastName, username: $.message.from.username, address: await Waves.createNewAddress()});
        }
        return true;
    }

    static async getUser($){
        let user = await User.findOne({where: {id: $.message.from.id}});
        if(user){
            return user
        }
        return null;
    }

    static async addAddress(user, address, name){
        return await AddressBook.create({user_id: user.id, address: address, name: name});
    }

    static async updateAddress(address, name){
        return await address.update({name: name});
    }

    static async updateFirstName(user, name){
        return await user.update({first_name: name})
    }

    static async updateLastName(user, name){
        return await user.update({last_name: name})
    }

    static async updateUsername(user, name){
        return await user.update({username: name})
    }

    static async deleteUser(user){
        let addresses = await AddressBook.findAll({where: {user_id: user.id}});
        for(let i=0; i< addresses.length; i++)
            addresses[i].destroy();
        await user.destroy();
    }

    static async deleteAddress(address){
        address.destroy();
    }

    static async getUserAddressBook(user){
        let data = await AddressBook.findAll({where: {user_id: user.id}});
        console.log(data);
        if(data)
            return data;
        return [];
    }
}
module.exports = UserManager;