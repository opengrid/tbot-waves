const config = require('../config');

const wavesNode = config.wavesNode;
const tokens = config.tokens;

const optionDefinitions = [
    { name: 'balance', alias: 'b', type: Boolean },
    { name: 'transfer', alias: 't', type: Boolean },
    { name: 'path', alias: 'p', type: String }
];
const commandLineArgs = require('command-line-args');
const options = commandLineArgs(optionDefinitions);
console.log(options);
const Transaction = require('./Controllers/transferManager.js');
const Balance = require('./Controllers/balanceManager.js');
const Token = require('./Controllers/tokenManager.js');

class WavesManager{
    constructor(){
    }

    makeTransactionsFromFile(path){
        Transaction.makeTransactionsFromFile(path);
        console.log('Transactions sent');
    }

    getTotalBalance(){
        Balance.getAssetsBalance(wavesNode.mainAddress);
        Balance.getWavesBalance(wavesNode.mainAddress);
    }
}
module.exports = WavesManager;
let wM = new WavesManager();
if(options.balance){
    wM.getTotalBalance();
}
else if(options.transfer){
    if(options.path) {
        wM.makeTransactionsFromFile(path)
    }else{
        console.log('Wrong path to file. Try -p "path"');
    }
}
else{
    console.log('Choose function (Available options -b, -t)');
}