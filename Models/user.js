module.exports = (sequelize, DataTypes) => {
    return sequelize.define('user', {
        id: { type: DataTypes.INTEGER, primaryKey: true, unique: true},
        first_name: { type: DataTypes.STRING },
        last_name: { type: DataTypes.STRING },
        username: { type: DataTypes.STRING, unique: true},
        address: { type: DataTypes.STRING, unique: true}
    });
};

