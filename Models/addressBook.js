module.exports = (sequelize, DataTypes) => {
    return sequelize.define('address_book', {
        user_id: { type: DataTypes.INTEGER,
            references: {
                model: 'users',
                key: 'id',
            }},
        name: { type: DataTypes.STRING },
        address: { type: DataTypes.STRING, unique: true}
    });
};

