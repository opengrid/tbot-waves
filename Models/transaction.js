module.exports = (sequelize, DataTypes) => {
    return sequelize.define('transaction', {
        transactionId: { type: DataTypes.STRING},
        sender_id: { type: DataTypes.INTEGER,
            references: {
                model: 'users',
                key: 'id',
            }},
        sender: { type: DataTypes.STRING},
        recipient: { type: DataTypes.STRING},
        amount: { type: DataTypes.BIGINT},
        assetId: {type: DataTypes.STRING},
        attachment: { type: DataTypes.STRING}
    });
};