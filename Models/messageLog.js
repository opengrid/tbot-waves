module.exports = (sequelize, DataTypes) => {
    return sequelize.define('messageLog', {
        message: {type: DataTypes.STRING},
        userId: {type: DataTypes.INTEGER}
})};