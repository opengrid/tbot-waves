module.exports = (sequelize, DataTypes) => {
    return sequelize.define('token', {
        assetId: {type: DataTypes.STRING, unique: true},
        assetName: {type: DataTypes.STRING}
    })
};