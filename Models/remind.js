module.exports = (sequelize, DataTypes) => {
    return sequelize.define('remind', {
        message: {type: DataTypes.TEXT},
        time: {type: DataTypes.DATE}
    })
};