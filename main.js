const config = require('./config');
const schedule = require('node-schedule');

const User = require('./Controllers/userManager.js');
const MessageLog = require('./Controllers/messageLogManager.js');
const Message = require('./Controllers/messageManager');

const Telegram = require('telegram-node-bot');
const TelegramBaseController = Telegram.TelegramBaseController;
const TextCommand = Telegram.TextCommand;
const tg = new Telegram.Telegram(config.bot.apiKey,{
    workers: 1
});

class StartController extends TelegramBaseController {

    startMenu($) {
        MessageLog.logData($);
        Message.startMenu($);
    }

    test($) {
        MessageLog.logData($);
        Message.testCommand($);
    }

    get routes() {
        return {
            'startCommand': 'startMenu',
            'testCommand': 'test'
        }
    }
}
class MessageController extends TelegramBaseController {
    help($) {
        MessageLog.logData($);
        const msg = '';
        $.sendMessage(msg);
    }

/*    async botMessage($){
        MessageLog.logData($);
        Message.messageById(tg,$);
    }

    async airdrop($){
        MessageLog.logData($);
        Transaction.airDrop($);
    }*/

    get routes() {
        return {
            'helpCommand': 'help',
            'messageCommand': 'botMessage',
            'groupMessageCommand': 'botGroupMessage',
            'airdropCommand': 'airdrop',
        }
    }
}


class AccountController extends TelegramBaseController {
    accountMenu($) {
        MessageLog.logData($);
        User.menu($);
    }
    get routes() {
        return {
            'accountCommand': 'accountMenu',
        }
    }
}

class OtherwiseController extends TelegramBaseController {
    handle($) {
        MessageLog.logData($);
    }
}

tg.router
    .when(new TextCommand('/help', 'helpCommand'), new MessageController())
    .when(new TextCommand('/start', 'startCommand'), new StartController())
    .when(new TextCommand('/menu', 'startCommand'), new StartController())
    .when(new TextCommand('/test', 'testCommand'), new StartController())
    .when(new TextCommand('/msg', 'messageCommand'), new MessageController())
    .when(new TextCommand('/groupMsg', 'groupMessageCommand'), new MessageController())
    .when(new TextCommand('/account', 'accountCommand'), new AccountController())
    .otherwise(new OtherwiseController());

//tg.onMaster(() => {schedule.scheduleJob('/1 * * * * *', () => Message.reminder(tg))});